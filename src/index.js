
function searchMovie() {
    $('#movie-list').html('');

    $.ajax({
        url: 'https://api.themoviedb.org/3/search/movie?',
        type: 'get',
        dataType: 'json',
        data: {
            'api_key': 'b8f0c6bc1268132afd6dd3849c85d93c',
            'query': $('#search-input').val(),
        },
        success: function (result) {
            // console.log(result);
            if (result.total_results > 0) {
                let movies = result.results;

                $.each(movies, function (i, data) {
                    $('#movie-list').append(`
                        <div class="col-md-4">
                            <div class="card mb-3 " style="height: 22rem;">
                                <img src="https://image.tmdb.org/t/p/w500/${data.backdrop_path}" class="card-img-top" alt="...">
                                <div class="card-body">
                                <h5 class="card-title">${data.title}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">${data.release_date}</h6>
                                <a href="#" class="card-link see-detail" data-bs-toggle="modal" data-bs-target="#exampleModal" data-id="${data.id}">See Detail</a>
                                </div>
                            </div>
                        </div>
                    `);
                });

                $('#search-input').val('');

            } else {(result.total_results == 0) 
                $('#movie-list').html(`
                    <div class="col">
                        <h1 class="text-center">` + `Movie Not Found` + `</h1>
                    </div>
                `)
            }
        }
    });
}

$('#search-button').on('click', function () {
    searchMovie();
});

$('#search-input').on('keyup', function (e) {
    if (e.which === 13) {
        searchMovie();
    }
});



$('#movie-list').on('click', '.see-detail', function () {
    const api = 'https://api.themoviedb.org/3/movie/';
    const img_api = 'https://image.tmdb.org/t/p/w500/';
    let m_id = $(this).data('id');

    $.ajax({
        url: api + m_id,
        dataType: 'json',
        type: 'get',
        data: {
            'api_key': 'b8f0c6bc1268132afd6dd3849c85d93c', 
        },
        success: function (detail) {
                $('.modal-body').html(`
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="`+ img_api +``+ detail.poster_path + `" class="img-fluid">
                            </div>

                            <div class="col-md-8">
                                <ul class="list-group">
                                    <li class="list-group-item"><h3>`+ detail.title +`</h3></li>
                                    <li class="list-group-item">Overview : `+ detail.overview+` </li>                 
                                    <li class="list-group-item">Released : `+ detail.release_date +`</li>
                                    <li class="list-group-item">Rating : `+ detail.vote_average +`</li>                             
                                </ul>
                            </div>
                        </div>
                    </div>
                `);
        }
    });

});
